/*
 * Go Terminal - 字串檔案
 * 您可自由修改此處所有內容
 * 語言：en_US.UTF-8
 */

package main
 
import "fmt"

const (
  Version = "v0.2.0-alpha"
  Authors = "pan93412"
)

/** main.go **/

// main.go: process(): 使用者輸入 about 時
// 第一個 %s: GoTerminal 版本
// 第二個 %s: 作者列表
var aboutTxt = fmt.Sprintf(`  -- Go Terminal --

+-----
|           
+____+ +----+  
|    | |    |   _ > TERMINAL _
+----+ +----+                     

Version: %s
Authors: %s

   / IMPROVE ME  \
   \ ~~~~~~~~~~~ /
      +---> http://www.github.com/
         -> pan93412/GoTerminal
`, Version, Authors)

// main.go: process(): 輸入 help 出現的指令清單
var helpTxt = ` == GoTerminal COMMANDS ==
 help:   Show the help text
 about:  Show the about text
 intro:  Show the intro text
 update: Update System
 exit:   Leave the shell
 ls:     Show the current directory content.
 su:     Switch to root mode.
 info:   System Info.
 rm:     Remove files or directories.
 ???     Easter egg. Try to find it without read source code!
         BTW, ??? isn't a command. :-)
`
// main.go: process(): 輸入 update 時
var updateTxt = "ERROR: No Internet Connection!"

// main.go: process(): 輸入 exit 時
var bye = "INFO: Send POWEROFF request."

// main.go: process(): 輸入 ls 時
var lsTxt = `[/]
/bin /sys (./TestSystem)`

// main.go: process(): 輸入 info 時
var infoTxt = ` == GoTerminal INFO ==

CPU:  VAX-11/780@5MHz
RAM:  15.9MiB (Used: %dKB)
USER: %s
VER:  %s, release at 1984-11-13.
COMPATIBLE MODE: DOS, GOTERM.MODE
`

// main.go: process(): 當使用 ./TestSystem 病毒時，記憶體佔用大於 4MB 時顯示。
var RAMFull = "ERROR: RAM is full. You can't do anything now. Try force-restart?"

// main.go: process(): 輸入 rm 時
var invaildRm = "Usage: rm -rf /"

// main.go: process(): 為 root 模式下輸入 su 時。
var sued = "ERROR: You are \"root\" now!"

// main.go: process(): 輸入 rm -rf / 時
var kernelErr = `  !!!KERNEL PANIC!!!
REASON: UNEXCEPTED PREEMPTION

 REGISTER:
 R0= 0x1C0F842D R1= 0xFF0F25B0
 R2= 0x1F4620FF R3= 0xCC0DF497
 R4= 0x1C1E2264 R5= 0x1CC190DA
 R6= 0x21B3BBBB R7= 0x10001000
 R8= 0x00000000 R9= 0x000CCC00
 R10=0x101FC456 R11=0x1F1C8103
 AP= 0xFFFF7000 FP= 0x17770049
 SP= 0x17770042 PC= 0x0xFF3174

 STACK TRACE:
 [0xFF380000]  panic();
 [0xFF317484]  schedule();
 [0xFF247A0C]  getaddr();
 [0xFF37F252]  mscprdsk();
 [0xFF2475BB]  syscall();

 LAST USED DATA:
 [0x00106600]  "/sys.clrl (r10)+"

 LAST SYSCALL:
 [0x00177708]  READ(84, 0x106600, 40);


SYSTEM HALTED.`

// main.go: process(): 主機名稱
var hostname = "ucbvax"
// main.go: process(): 輸入 ./TestSystem 時
var testTxt = "YOUR SYSTEM HAS BEEN DESTROY BY ME! HAHAHA!!!!"

// main.go: process(): 輸入 ./TestSystem 之後對 prompt 修改的文字
var promptHacked = "HACKED - by @viavivo, 1985. | "

// main.go: process(): 若指令不存在時
// 第一個 %s：輸入的指令
var unknownCommand = "[GoTerminal] Command not found: %s\n"

// main.go: main(): 初始畫面
// 第一個 %s: GoTerminal 版本
var introScreen = fmt.Sprintf(`[GoTerminal %s]

 -> Input "help" to know how to use the terminal!
  # The terminal still testing.

`, Version)

// main.go: main(): 彩蛋
var egg = "You found it! Good!"
