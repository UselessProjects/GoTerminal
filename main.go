// GoTerminal is a pseduo terminal, it
// can give you 80s terminal style!
//
// ... But the 80s terminal run
// on the modern computer
//     ... Cool?
//         ... But old PC can't run it...
//             ... SO FAKE XD
//
// Language: en_US.UTF-8.
// Author:   pan93412 & Neo_Chen
// TEXTEDIT: gedit 3.30.1
//
package main
import (
  "fmt"              // Handle input & output
  "os"               // os.Stdin & more
  "math/rand"        // Generate a random number
  "time"             // Show the current time
  "bufio"            // Customize "fmt.Scanln"
  "strings"          // Replace strings.
)

var addValue int = 20
var erred bool = false

// RAMHack is a function, which can add [addInt]
// to [addValue] every 500 millisecond.
func RAMHack(addInt int) {
  for {
    time.Sleep(500 * time.Millisecond)
    addValue += addInt
  }
}

// process is a handler, which can handle the
// command, and do the assigned action.
func process(prompt *string, inputContent string) string {
  if addValue >= 16384 {return RAMFull}
  rootMode := hostname + "# "
  userMode := hostname + "$ "
  switch inputContent {
    case "help": return helpTxt           // help COMMAND
    case "su":                            // su COMMAND
      if *prompt == rootMode {
        return sued
      }
      *prompt = rootMode
      return ""
    case "ls": return lsTxt               // ls COMMAND       
    case "about": return aboutTxt         // about COMMAND
    case "intro": return introScreen      // intro COMMAND
    case "update": return updateTxt       // update COMMAND
    case "./TestSystem":                  // ./TestSystem EXECUTEION FILE
      go RAMHack(200)
      if !erred {
        *prompt = promptHacked + *prompt
        erred = true
      }
      return testTxt
    case "rm": return invaildRm           // rm COMMAND
    case "rm -rf /":                      // rm -rf / COMMAND
          fmt.Println(kernelErr)
      for {
        time.Sleep(30 * time.Second)
      }
    case "": return ""                    // IF USER JUST "Enter"
    case "info":                          // info COMMAND
      if *prompt == rootMode {
        return fmt.Sprintf(infoTxt, rand.Intn(20) + addValue, Version)
      }
      return fmt.Sprintf(infoTxt, rand.Intn(20) + addValue, Version)
    case "exit":                          // exit COMMAND
      if *prompt == rootMode {
        *prompt = userMode
        return ""
      }
      fmt.Println(bye)
      os.Exit(0)
    case "egg:10d001": return egg        // :-)
                                         // THE BELOW: INVAILD COMMAND
    default: return fmt.Sprintf(unknownCommand, inputContent)
  }
  return ""
}

func main() {
  rand.Seed(time.Now().Unix())
  var inputContent string

  fmt.Println(introScreen)
  prompt := hostname + "$ "
  osStdin := bufio.NewReader(os.Stdin)

  for {
    inputContent = ""
    fmt.Print(prompt)
    inputContent, _ = osStdin.ReadString('\n')
    inputContent = strings.Replace(inputContent, "\n", "", -1)
    fmt.Println(process(&prompt, inputContent))
  }
}
