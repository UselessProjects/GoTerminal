.PHONY: all
.PHONY: run
.PHONY: clean

# First  Delete the exist GoTerm.run.
# Second Build GoTerm.run from main.go & str.go
all: clean main.go str.go
	go build -o GoTerm.run main.go str.go

# Delete the exist GoTerm.run, for all.
clean:
	rm -rf GoTerm.run

# Do everything "all" does, and execute
# GoTerm.run.
run: all
	./GoTerm.run
	
	
# AUTHOR: pan93412
# DATE:   Mon Oct. 22 2018 23:42
