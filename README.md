# GoTerminal - a terminal with 80s style
## Terminalshot (MIGHT NOT LATEST)
```
[GoTerminal v0.2.0-alpha]

 -> Input "help" to know how to use the terminal!
  # The terminal still testing.


ucbvax$ help
 == GoTerminal COMMANDS ==
 help:   Show the help text
 about:  Show the about text
 intro:  Show the intro text
 update: Update System
 exit:   Leave the shell
 ls:     Show the current directory content.
 su:     Switch to root mode.
 info:   System Info.
 rm:     Remove files or directories.
 ???     Easter egg. Try to find it without read source code!
         BTW, ??? isn't a command. :-)

ucbvax$ 
```

## Authors
- pan93412, 2018.
- Neo_Chen, 2018.